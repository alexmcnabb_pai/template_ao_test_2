#include "eventbus.h"

ActiveObjectBase::ActiveObjectBase(std::string name) : name(name) {}

void ActiveObjectBase::run_wrapper(bool* keep_running) {
    run(keep_running);// Maybe add some exception handling here?
}

void ActiveObjectBase::subscribe(eventid_t event_id){
    EventBus::instance().subscription_map.emplace(event_id, std::vector<std::shared_ptr<ActiveObjectBase>>()); // Add empty vector if missing
    // cache these because we can't make pointers to the active object until after it's been constructed
    temp_sub_list.push_back(event_id);
}
void ActiveObjectBase::sendEvent(std::shared_ptr<BaseEvent> event) {
    for (std::shared_ptr<ActiveObjectBase> ao : EventBus::instance().subscription_map[event->id]) {
        ao->deliverEvent(event);
    }
}
void ActiveObjectBase::deliverEvent(std::shared_ptr<BaseEvent> event) {
    std::lock_guard lock(event_mutex);
    event_queue.push(event);
}
std::shared_ptr<BaseEvent> ActiveObjectBase::popEvent(uint64_t sleepTimeMilliseconds) {
    std::unique_lock<std::mutex> lock(event_mutex);
    auto until = std::chrono::system_clock::now() + std::chrono::milliseconds(sleepTimeMilliseconds);
    if (empty_cv.wait_until(lock, until, [&]{ return !event_queue.empty(); })) {
        auto ev = event_queue.front();
        event_queue.pop();
        return ev;
    } else {
        return std::make_shared<NoEvent>();
    }
}