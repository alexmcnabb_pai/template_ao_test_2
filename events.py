#!/usr/bin/env python3

# This file generates events.cpp and events.h, and is executed by cmake

cpp_header = """// This file is autogenerated by events.py. Changes made here will be lost!
#include "events.h"

int _event_index_counter = 0;
eventid_t BaseEvent::get_event_id() {
    return _event_index_counter++;
}
"""
h_header = """// This file is autogenerated by events.py. Changes made here will be lost!
#pragma once

typedef int eventid_t;

class BaseEvent {
public:
    BaseEvent(eventid_t id): id(id) {};
    const eventid_t id;
    static eventid_t get_event_id();
};
"""
cpp_event_template = """
const int {event_name}::EVENT_ID = BaseEvent::get_event_id();
{event_name}::{event_name}({args_with_types_string}) :
    BaseEvent({event_name}::EVENT_ID){args_initializers} {{}};
"""
h_event_template = """
class {event_name} : public BaseEvent {{
public:
    static const int EVENT_ID;
    {event_name}({args_with_types_string});{args_declarations}
}};
"""

print("Regenerating events.h and events.cpp")
cpp_file = open("events.cpp", 'w')
h_file = open("events.h", 'w')

cpp_file.write(cpp_header)
h_file.write(h_header)

def add_event(event_name, args):
    args_with_types_string = ", ".join([f"{vartype} {varname}" for varname, vartype in args.items()])
    args_initializers = "".join([f", {varname}({varname})" for varname in args.keys()])
    args_declarations = "".join([f"\n    const {vartype} {varname};" for varname, vartype in args.items()])
    cpp_file.write(cpp_event_template.format(event_name=event_name, args_with_types_string=args_with_types_string, args_initializers=args_initializers))
    h_file.write(h_event_template.format(event_name=event_name, args_with_types_string=args_with_types_string, args_declarations=args_declarations))


add_event("NoEvent", {})
add_event("TestEvent", {"param1": "int", "param2": "int"})
