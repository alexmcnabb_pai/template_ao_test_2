
#pragma once
#include <iostream>
#include <map>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <memory>

#include "events.h"

class ActiveObjectBase;

class EventBus {
public:
    // Singleton pattern
    EventBus(const EventBus&) = delete;
    EventBus& operator=(const EventBus&) = delete;
    static EventBus& instance() {
        static EventBus theInstance;                  
        return theInstance;
    }
    // This gets filled at construction time, so it's probably possible to make it all done at compile time to optimise stuff
    std::map<eventid_t, std::vector<std::shared_ptr<ActiveObjectBase>>> subscription_map;
    bool keep_running = true;
private:
    EventBus() {};
};

class ActiveObjectBase {
public:
    ActiveObjectBase(std::string name);
    void run_wrapper(bool* keep_running);
    virtual void run(bool* keep_running) {};
    void subscribe(eventid_t event_id); // This should only be called in the constructor
    void deliverEvent(std::shared_ptr<BaseEvent> event);
    void sendEvent(std::shared_ptr<BaseEvent> event);
    std::string name;
    std::vector<eventid_t> temp_sub_list;
    std::shared_ptr<BaseEvent> popEvent(uint64_t sleepTimeMilliseconds = 50);
    template<class Event, class... Args> void sendEvent(Args&&... args) {
        auto event = std::make_shared<Event>(args...);
        sendEvent(event);
    }
private:
    std::mutex event_mutex;
    std::queue<std::shared_ptr<BaseEvent>> event_queue;
    std::condition_variable empty_cv;
};


template<class AO>
class ActiveObjectTemplateClass {
public:
    explicit ActiveObjectTemplateClass<AO>(std::shared_ptr<AO> handler) : handler(handler) {
        thread = std::make_unique<std::thread>(&AO::run_wrapper, handler, &EventBus::instance().keep_running);
    }
    ~ActiveObjectTemplateClass() {
        std::cout << "Joining thread for " << handler->name <<std::endl;
        // Can't join the thread in the ActiveObjectBase destructor, because it'll run after the thread is already gone
        EventBus::instance().keep_running = false;
        if (thread->joinable()) thread->join();
    }
private:
    std::shared_ptr<AO> handler;
    std::unique_ptr<std::thread> thread;
};

template<class AO, class... Args>
std::shared_ptr<ActiveObjectTemplateClass<AO>> make_ao(Args&&... args) {
    std::shared_ptr<AO> handler = std::make_shared<AO>(args...);
    for (eventid_t event_id : handler->temp_sub_list) {
        EventBus::instance().subscription_map[event_id].push_back(std::shared_ptr<AO>(handler));
    }
    return std::make_shared<ActiveObjectTemplateClass<AO>>(handler);
}
