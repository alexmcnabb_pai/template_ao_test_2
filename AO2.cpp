#include <unistd.h>

#include "AO2.h"


AO2::AO2(std::string teststring) : ActiveObjectBase("AO2") {
    std::cout << "AO2 constructor\n";
    subscribe(TestEvent::EVENT_ID);
}

void AO2::run(bool* keep_running) {
    std::cout << "AO2 starting" << std::endl;
    while(*keep_running) {
        std::shared_ptr<BaseEvent> event = popEvent();
        if (event->id == TestEvent::EVENT_ID) {
            std::shared_ptr<TestEvent> data = std::static_pointer_cast<TestEvent>(event);
            std::cout << "Got test event with values " << data->param1 << " and " << data->param2 << std::endl;
        }
        sleep(1);
    }
    std::cout << "AO2 done" << std::endl;
}