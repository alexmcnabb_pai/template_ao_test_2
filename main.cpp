#include <iostream>
#include <string>
#include <unistd.h>
#include <map>
#include <vector>
#include <queue>
#include <condition_variable>
#include <iostream>
#include <thread>
#include <memory>

#include "events.h"
#include "eventbus.h"

#include "AO1.h"
#include "AO2.h"

int main(int argc, char* argv[]) {
    std::cout << "Starting construction\n";
    auto ao1 = make_ao<AO1>();
    auto ao3 = make_ao<AO2>("asdf");
    std::cout << "Done constructing\n";
    sleep(50); // This should really be a while(true) loop that exits on ctrl-c
    std::cout << "main done" << std::endl;
}
