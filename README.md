# How to build

```
mkdir build
cd build
cmake ..
make
```

### EventBus:
Singleton. Stores map of lists of pointers to each ActiveObject that is subscribed to a given message type. Also contains keep_running bool which tell ActiveObjects when to shut down

### ActiveObjectBase:
Each ActiveObject inherits from this. Contains a queue for incoming events, and a mutex for this queue. Has function to subscribe to events, this can only be used in the constructor. Has function to send an event, which will call the deliver function in the target activeobject via the subscription map in eventbus. Should always be created with `make_ao<AOType>()`, as this creates a thread for the object to run on, and ensures it will be cleaned up properly.

### Code generation:
`events.h` and `events.cpp` are generated from `events.py` automatically by cmake, to cut down on the boilerplate code required for every event.

### TODO:
- Check behavour when exceptions are raised
- Not sure how this is going to integrate into qt gui?
- Remove need to use ::EVENT_ID when subscribing to / receiving events (maybe not worth it)
- Improve functionality of code generation - allow specifying custom imports, optionally make second constructor with no args
- Add functionality for handling different overflow of events