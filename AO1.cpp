#include <unistd.h>

#include "AO1.h"

AO1::AO1() : ActiveObjectBase("AO1") {
    std::cout << "AO1 constructor\n";
}
void AO1::run(bool* keep_running) {
    std::cout << "AO1 starting\n";
    while(*keep_running) {
        std::cout << "AO1 looping\n";
        auto event = std::make_shared<TestEvent>(2, 9);
        sendEvent(event);
        sendEvent<TestEvent>(3, 8);
        sleep(1);
    }
    std::cout << "AO1 done" << std::endl;
}
