#pragma once
#include "eventbus.h"

class AO2 : public ActiveObjectBase{
public:
    AO2(std::string teststring);
    void run(bool* keep_running);
};